package behavioral.obserwer.Game;

public abstract class Game {

    void initialize() {
        System.out.println("Przygotowanie zawodników rozgrzewka");
    }

    abstract void prepereField();

    void play(){
        System.out.println("Let's play!");
    }

    abstract void endGame();

    void startGame(){
        initialize();
        prepereField();
        play();
        endGame();
    }
}
