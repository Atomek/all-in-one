package behavioral.obserwer.iterator;

import java.util.*;

public class IteratorDemo {
    public static void main(String[] args) {
        List<String> testData = Arrays.asList("tets", "kot", "dom");
        Iterator<String> iterator = testData.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
        Set<String> testSet = new HashSet<>();
        testSet.add("me");
        testSet.add("se");

        printAllCollectionElements(testSet);
    }

    private static void printAllCollectionElements(Collection<String> collection){
        Iterator<String> iterator = collection.iterator();

        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
