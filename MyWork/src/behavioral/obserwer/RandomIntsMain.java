package behavioral.obserwer;

public class RandomIntsMain {
    public static void main(String[] args) {
        RandomIntGenerator randomIntGenerator = new RandomIntGenerator();
        ConsoleRandomIntObserver consoleRandomIntObserver = new ConsoleRandomIntObserver();
        randomIntGenerator.subscribe(consoleRandomIntObserver);


        FileRandomIntObserver fileRandomIntObserver = new FileRandomIntObserver();
        randomIntGenerator.subscribe(fileRandomIntObserver);
        randomIntGenerator.generateNumbers();
    }
}
