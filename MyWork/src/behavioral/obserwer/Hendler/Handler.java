package behavioral.obserwer.Hendler;

public interface Handler {
    int handle(int amountOfMoney);

    Handler nextHandler();
}
