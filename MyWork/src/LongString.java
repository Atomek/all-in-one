public class LongString {
    public static void main(String[] args) {
        String[] arrayString = {"aba",
                "aa",
                "ad",
                "vcd",
                "aba"};

        arrayString = allLongestString(arrayString);
        for (String arrayWords :
                arrayString) {
            System.out.println(arrayWords);
        }
    }

    private static String[] allLongestString(String[] arrayString) {
        int max = Integer.MIN_VALUE;

        for (int indexOfString = 0; indexOfString < arrayString.length; indexOfString++) {
            if (isBigger(max, arrayString[indexOfString].length())) {
                max = arrayString[indexOfString].length();
            }
        }


        return takeAllStringWithMaxLenght(max, arrayString);
    }

    private static boolean isBigger(int max, int length) {
        if (max < length) {
            return true;
        }
        return false;
    }

    private static String[] takeAllStringWithMaxLenght(int max, String[] arrayString) {
        int counter = 0;
        int invalidData = 0;
        for (int index = 0; index < arrayString.length; index++) {
            if (arrayString[index].length() == max) {
                counter++;
            }
        }String[] strings1 = new String[counter];
        for (int newIndex = 0; newIndex < arrayString.length; newIndex++) {
            if (arrayString[newIndex].length() == max) {
                strings1[invalidData] = arrayString[newIndex];
                invalidData++;
            }
        }
        return strings1;
    }
}
