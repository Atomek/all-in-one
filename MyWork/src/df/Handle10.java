package df;

import behavioral.obserwer.Hendler.Handler;

public class Handle10 implements Handler {
    Handler handler;

    public Handle10(Handler handler) {
        this.handler = handler;
    }

    @Override
    public int handle(int amountOfMoney) {
        System.out.println("10: "+amountOfMoney);
        return amountOfMoney%10;
    }

    @Override
    public Handler nextHandler() {
        return handler;
    }
}