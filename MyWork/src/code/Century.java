package code;

public class Century {
    public static void main(String[] args) {
        System.out.println(centuryFromYear(1993));
        System.out.println(centuryFromYear(1901));
        System.out.println(centuryFromYear(1900));
        System.out.println(centuryFromYear(13));
        System.out.println(centuryFromYear(8));
    }

    static int centuryFromYear(int year) {
        int century = 0;
        Integer rok = year;
        String stringYear =  rok.toString();


        if(year<101){
            return century+1;
        }
        if (stringYear.charAt(stringYear.length()-2)=='0' && stringYear.charAt(stringYear.length()-1)=='0'){
            century=century-1;
        }
        for (int index = 0; index <= stringYear.length() - 3; index++) {
            century= (int) ((stringYear.charAt(index)-48)*Math.pow(10,stringYear.length()-3-index)) + century;
        }



        return century+1;
    }

}
