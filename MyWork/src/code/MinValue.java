package code;

public class MinValue {
    public static void main(String[] args) {
        System.out.println(adjacentElementsProduct(new int[]{2, 3, 4,5,6,7}));
    }

    static int adjacentElementsProduct(int[] inputArray) {
        int max = Integer.MIN_VALUE;
        int result;
        for (int index = 0; index < inputArray.length - 1; index++) {
            result=inputArray[index]*inputArray[index+1];
            if(result>max){
                max = result;
            }
        }
        return max;
    }
}
