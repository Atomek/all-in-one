package service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.javamoney.moneta.Money.*;

public class ProductStore {
    static List<Product> listOfProductInStore = new ArrayList<>();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int choice;
        do {
            System.out.println("Witaj! \n1. Add product to sell \n2. Remove product from list\n" +
                    "3. Show all products\n4. Modify your Product\n0.Finish");
            choice = scanner.nextInt();
            yourChoice(choice);
        } while (choice != 0);
    }

    private static void yourChoice(int choice) {
        switch (choice) {
            case 1:
                addProduct();
                break;
            case 2:
                removeProduct();
                break;
            case 3:
                showAllItem();
                break;
            case 4:
                modify();
                break;
        }
    }

    private static void modify() {
        showAllItem();
        System.out.println("Chose product");
        int index = scanner.nextInt();
        System.out.println("What you want to change?\n1.Price\n2.Kind Of Product\n 3.Color\n4. Weight");
        int modifyChoice = scanner.nextInt();

        switch (modifyChoice) {
            case 1: {
                System.out.println("New price and code");
                int price = scanner.nextInt();
                String code = scanner.next();
                listOfProductInStore.get(index).setPrice(of(price, code));
                break;
            }
            case 2: {
                System.out.println("New name");
                String name = scanner.next();
                listOfProductInStore.get(index).setKindOfProduct(name);
                break;
            }
            case 3: {
                System.out.println("Change Color");
                String color = scanner.next();
                listOfProductInStore.get(index).setColor(color);
                break;
            }
            case 4: {
                System.out.println("New Weight");
                double weight = scanner.nextDouble();
                listOfProductInStore.get(index).setWeight(weight);
                break;
            }
        }
    }

    private static void showAllItem() {
        if (listOfProductInStore.size() == 0) {
            System.out.println("Your shop is empty");
            return;
        }
        int indexOfProduct = 0;
        for (Product listOfItem :
                listOfProductInStore) {
            System.out.printf("%4d" + "%8s\t" + "%4.4f" + "%8s" + "%8s\n", indexOfProduct, listOfItem.getPrice(),
                    listOfItem.getWeight(), listOfItem.getKindOfProduct(), listOfItem.getColor());
            indexOfProduct++;
        }


    }

    private static void removeProduct() {
        if (listOfProductInStore.size() == 0) {
            System.out.println("Your shop is empty");
            return;
        }
        System.out.println("What you want to remove");
        int toRemove = scanner.nextInt();
        listOfProductInStore.remove(toRemove);
    }

    private static void addProduct() {
        System.out.println("Hello in add Product. Set basic information about it. ");
        System.out.println("Kind of Product");
        String kindOfProduct = scanner.next();
        System.out.println("Price and code");
        int price = scanner.nextInt();
        String code = scanner.next();
        System.out.println("Color");
        String color = scanner.next();
        System.out.println("Weight");
        double weight = scanner.nextDouble();
        System.out.println("How many");
        int howMany = scanner.nextInt();

        for (int counterOfPieces = 0; counterOfPieces < howMany; counterOfPieces++) {
            Product product = new Product(of(price, code), kindOfProduct, color, weight);
            listOfProductInStore.add(product);
        }

    }
}
