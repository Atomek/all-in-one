package com.example.demo.repositories;

import com.example.demo.Entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepositiory extends JpaRepository<Employee, Long> {

}
