package com.example.demo.Entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data       //tworzy getery i setery z automatu
public class Employee {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private String role;

    protected Employee(){}

    public Employee(String name, String role){
        this.name = name;
        this.role = role;
    }
}
