package com.example.demo.Controllers;

import com.example.demo.Entities.Employee;
import com.example.demo.repositories.EmployeeRepositiory;
import org.springframework.remoting.RemoteTimeoutException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {
    private final EmployeeRepositiory repositiory;

    public EmployeeController(EmployeeRepositiory repositiory) {
        this.repositiory = repositiory;
    }
    @GetMapping("/employees")
    public List<Employee> getEmployees(){
        return repositiory.findAll();
    }
    @PostMapping("/employees")
    public Employee newEmployee(@ModelAttribute Employee employee){
        return repositiory.save(employee);
    }

    @GetMapping("/employees/{id}")
    public Employee getEmployee(@PathVariable Long id){
        return repositiory.findById(id).orElseThrow(()-> new RemoteTimeoutException("nie ma"));
    }
    @DeleteMapping("/employees/{id}")
    public void removeEmployee(@PathVariable Long id){
        repositiory.deleteById(id);
    }
    @PutMapping("/employees/{id}")
    public Employee updateEmployee(@ModelAttribute Employee employeeOld, @PathVariable Long id){
        return repositiory.findById(id)
                .map(employee -> {
                    employee.setName(employeeOld.getName());
                    employee.setRole(employeeOld.setRole());
                return repositiory.save(employee);});

}

}
