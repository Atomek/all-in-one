import java.util.Scanner;

public class ArrayWithNumbers {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        Start();
    }

    private static void Start() {
        int[][] array = createArray();
        putNumbers(array);
        printArray(array);
    }

    private static int[][] createArray() {
        int sizeN;
        System.out.println("Size n:n");
        sizeN = scanner.nextInt();
        return new int[sizeN][sizeN];
    }

    private static void putNumbers(int[][] array) {
        for (int counterX = 0; counterX < array.length; counterX++) {
            for (int counterY = 0; counterY < array[counterX].length; counterY++) {
                array[counterX][counterY] = counterX + counterY + 1;
                if (array[counterX][counterY] > array.length){
                    array[counterX][counterY] = array[counterX][counterY] - array.length ;
                }
            }
        }
    }

    private static void printArray(int[][] array) {
        for (int counterX = 0; counterX < array.length; counterX++) {
            for (int counterY = 0; counterY < array[counterX].length; counterY++) {
                System.out.print(array[counterX][counterY]);
            }
            System.out.println();
        }
    }
}
