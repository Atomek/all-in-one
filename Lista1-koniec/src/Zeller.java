
import java.util.Scanner;

public class Zeller {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("YYYY-MM-DD");
        int year = scanner.nextInt();
        int month = changeNumber(scanner.nextInt());
        int day = scanner.nextInt();

        int h = Math.floorMod((int) (day+(26*(month+1)/10)+(1.25*Math.floorMod(year,100))+(5.25*(year/100))),7);
        if (month == 13 || month == 14)
            h = h-1;

        _6DzienTygodnia(h);
    }

    private static void _6DzienTygodnia(int h) {
        switch (h){
            case 0:
                System.out.println("Saturday");
                break;
            case 1:
                System.out.println("SunDay");
                break;
            case 2:
                System.out.println("Monday");
                break;
            case 3:
                System.out.println("Tuesday");
                break;
            case 4:
                System.out.println("Wednesday");
                break;
            case 5:
                System.out.println("Thursday");
                break;
            case 6:
                System.out.println("Friday");
        }
    }

    private static int changeNumber(int month) {
        if (month ==1 || month == 2){
            return month+12;
        }
        return month;
    }
}
