import java.util.Random;
import java.util.Scanner;

public class Saper {
    static Scanner scanner = new Scanner(System.in);
    static Random random = new Random();

    public static void main(String[] args) {

        System.out.println("Podaj wymiar tablicy");
        int rows = scanner.nextInt();
        int columns = scanner.nextInt();

        System.out.println("Prawodopodobieństwo");
        int p = scanner.nextInt();

        Boolean[][] array = new Boolean[rows][columns];
        double prawdo;
        do {
            array[random.nextInt(rows)][random.nextInt(columns)] = true;
            prawdo = resultIt(array);
        } while (p > prawdo);

        printArray(array);

        String[][] straras = putStars(array, rows, columns);
        System.out.println();
        printArray(howManyStars(straras));

    }


    private static int isExist(String[][] integers, int i, int j) {
        try {
            if (integers[i][j] == "*") {
                return 1;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }
        return 0;
    }


    private static String[][] howManyStars(String[][] straras) {

        for (int i = 0; i < straras.length; i++) {
            for (int j = 0; j < straras[i].length; j++) {
                int result = 0;
                if (straras[i][j] != "*") {
                    result = isExist(straras, i - 1, j)
                            + isExist(straras, i + 1, j)
                            + isExist(straras, i, j + 1)
                            + isExist(straras, i, j - 1)
                            + isExist(straras, i - 1, j - 1)
                            + isExist(straras, i - 1, j + 1) +
                            isExist(straras, i + 1, j + 1) + isExist(straras, i + 1, j - 1);


                    straras[i][j] = Integer.toString(result);
                }
            }

        }
        return straras;
    }

    private static void printArray(Boolean[][] array) {
        for (int counterX = 0; counterX < array.length; counterX++) {
            for (int counterY = 0; counterY < array[counterX].length; counterY++) {
                System.out.printf("%6s", array[counterX][counterY]);
            }
            System.out.println();
        }
    }

    private static void printArray(String[][] array) {
        for (int counterX = 0; counterX < array.length; counterX++) {
            for (int counterY = 0; counterY < array[counterX].length; counterY++) {
                System.out.printf("%6s", array[counterX][counterY]);
            }
            System.out.println();
        }
    }

    private static String[][] putStars(Boolean[][] array, int rows, int columns) {
        String[][] arrayWithStrings = new String[rows][columns];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] != null) {
                    arrayWithStrings[i][j] = "*";
                }
            }
        }
        return arrayWithStrings;
    }


    private static double resultIt(Boolean[][] array) {
        int result = 0;
        int size = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                size++;
                if (array[i][j] != null) {
                    result++;
                }
            }
        }
        return result * 100 / size;
    }
}