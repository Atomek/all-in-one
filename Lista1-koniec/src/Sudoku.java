public class Sudoku {
    private static int[][] sudoku9x9 = new int[9][9];
    private static int[][] cwiartka1 = new int[3][3];
    private static int[][] cwiartka2 = new int[3][3];
    private static int[][] cwiartka3 = new int[3][3];
    private static int[][] cwiartka4 = new int[3][3];
    private static int[][] cwiartka5 = new int[3][3];
    private static int[][] cwiartka6 = new int[3][3];
    private static int[][] cwiartka7 = new int[3][3];
    private static int[][] cwiartka8 = new int[3][3];
    private static int[][] cwiartka9 = new int[3][3];

    public static void main(String[] args) {
        for (int i = 0; i < sudoku9x9.length; i++) {
            for (int j = 0; j < sudoku9x9[i].length; j++) {
                sudoku9x9[i][j]=0;
            }
        }

        startPoints();
        refresh();

        for (int i = 1; i <= 9; i++) {
            putNumbertoSudokuArray(i);
        }
        printArray(sudoku9x9);

    }

    private static void refresh() {
        refreshCwiartka1();
        refreshCwiartka2();
        refreshCwiartka3();
        refreshCwiartka4();
        refreshCwiartka5();
        refreshCwiartka6();
        refreshCwiartka7();
        refreshCwiartka8();
        refreshCwiartka9();
    }

    private static void refreshCwiartka1() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                cwiartka1[i][j]=sudoku9x9[i][j];
            }
        }
    }
    private static void refreshCwiartka4() {
        int number = 0;
        for (int i = 3; i < 6; i++) {
            for (int j = 0; j < 3; j++) {
                cwiartka4[number][j]=sudoku9x9[i][j];
            }number++;
        }
    }
    private static void refreshCwiartka2() {
        for (int i = 0; i < 3; i++) {
            int number = 0;
            for (int j = 3; j < 6; j++) {
                cwiartka2[i][number]=sudoku9x9[i][j];
                number++;
            }
        }
    }
    private static void refreshCwiartka3() {
        for (int i = 0; i < 3; i++) {
            int number = 0;
            for (int j = 6; j < 9; j++) {
                cwiartka3[i][number]=sudoku9x9[i][j];
                number++;
            }
        }
    }
    private static void refreshCwiartka5() {
        int numberX = 0;
        for (int i = 3; i < 6; i++) {
            int numberY = 0;
            for (int j = 3; j < 6; j++) {
                cwiartka5[numberX][numberY]=sudoku9x9[i][j];
                numberY++;
            }numberX++;
        }
    }
    private static void refreshCwiartka6() {
        int numberX = 0;
        for (int i = 3; i < 6; i++) {
            int numberY = 0;
            for (int j = 6; j < 9; j++) {
                cwiartka6[numberX][numberY]=sudoku9x9[i][j];
                numberY++;
            }numberX++;
        }
    }
    private static void refreshCwiartka7() {
        int numberX = 0;
        for (int i = 6; i < 9; i++) {
            int numberY = 0;
            for (int j = 0; j < 3; j++) {
                cwiartka7[numberX][numberY]=sudoku9x9[i][j];
                numberY++;
            }numberX++;
        }
    }
    private static void refreshCwiartka8() {
        int numberX = 0;
        for (int i = 6; i < 9; i++) {
            int numberY = 0;
            for (int j = 3; j < 6; j++) {
                cwiartka8[numberX][numberY]=sudoku9x9[i][j];
                numberY++;
            }numberX++;
        }
    }
    private static void refreshCwiartka9() {
        int numberX = 0;
        for (int i = 6; i < 9; i++) {
            int numberY = 0;
            for (int j = 6; j < 9; j++) {
                cwiartka9[numberX][numberY]=sudoku9x9[i][j];
                numberY++;
            }numberX++;
        }
    }

    private static void putNumbertoSudokuArray(int number) {
        for (int sizeX = 0; sizeX < sudoku9x9.length; sizeX++) {
            for (int sizeY = 0; sizeY < sudoku9x9[sizeX].length; sizeY++) {
                if (sudoku9x9[sizeX][sizeY]== 0){
                if(isNumberInColumns(number,sizeY) && isNumberInRows(number,sizeX) && !isInSmallArray(number, sizeX, sizeY)){
                    sudoku9x9[sizeX][sizeY]=number;
                    refresh();
                }}
            }
        }
    }

    private static boolean isInSmallArray(int number, int sizeX, int sizeY) {
        if (sizeX < 3){
            if (sizeY <3){
                if(numberIsInArray(number, cwiartka1));{
                    return true;
                }
            }
        }
        if (sizeX < 3){
            if (sizeY <6){
                if(numberIsInArray(number, cwiartka2));{
                    return true;
                }
            }
        }
        if (sizeX < 3){
            if (sizeY <9){
                if(numberIsInArray(number, cwiartka3));{
                    return true;
                }
            }
        }
        if (sizeX < 6 && sizeX>=3){
            if (sizeY <3){
                if(numberIsInArray(number, cwiartka4));{
                    return true;
                }
            }
        }
        if (sizeX < 6 && sizeX>=3){
            if (sizeY <6 && sizeY>=3){
                if(numberIsInArray(number, cwiartka5));{
                    return true;
                }
            }
        }
        if (sizeX < 6 && sizeX>=3){
            if (sizeY <9 && sizeY>=6){
                if(numberIsInArray(number, cwiartka6));{
                    return true;
                }
            }
        }
        if (sizeX < 9 && sizeX>=6){
            if (sizeY <9 && sizeY>=6){
                if(numberIsInArray(number, cwiartka9));{
                    return true;
                }
            }
        }
        if (sizeX < 9 && sizeX>=6){
            if (sizeY <3 && sizeY>=0){
                if(numberIsInArray(number, cwiartka8));{
                    return true;
                }
            }
        }
        if (sizeX < 9 && sizeX>=6){
            if (sizeY <6 && sizeY>=3){
                if(numberIsInArray(number, cwiartka7));{
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean numberIsInArray(int number, int[][] cwiartka) {
        for (int i = 0; i < cwiartka.length; i++) {
            for (int j = 0; j < cwiartka[i].length; j++) {
                if (cwiartka[i][j]==number){
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean isNumberInColumns(int number, int sizeY) {
        for (int counterX = 0; counterX < 9; counterX++) {
            if (sudoku9x9 [counterX][sizeY]== number)
                return false;
        }
        return true;
    }

    private static boolean isNumberInRows(int number, int sizeX) {
        for (int counterY = 0; counterY < sudoku9x9.length; counterY++) {
            if (sudoku9x9[sizeX][counterY] == number)
                return false;
        }
        return true;
    }

    private static void startPoints() {
        sudoku9x9[0][0] = 2;
        sudoku9x9[0][4] = 3;
        sudoku9x9[0][6] = 1;
        sudoku9x9[1][4] = 6;
        sudoku9x9[1][5] = 5;
        sudoku9x9[1][6] = 8;
        sudoku9x9[2][0] = 7;
        sudoku9x9[2][1] = 1;
        sudoku9x9[2][3] = 8;
        sudoku9x9[3][1] = 8;
        sudoku9x9[3][5] = 4;
        sudoku9x9[3][6] = 2;
        sudoku9x9[3][8] = 1;
        sudoku9x9[4][1] = 6;
        sudoku9x9[4][7] = 7;
        sudoku9x9[5][0] = 1;
        sudoku9x9[5][2] = 7;
        sudoku9x9[5][3] = 2;
        sudoku9x9[5][7] = 3;
        sudoku9x9[6][5] = 7;
        sudoku9x9[6][7] = 1;
        sudoku9x9[6][8] = 2;
        sudoku9x9[7][2] = 1;
        sudoku9x9[7][3] = 9;
        sudoku9x9[7][4] = 2;
        sudoku9x9[8][2] = 5;
        sudoku9x9[8][4] = 8;
        sudoku9x9[8][8] = 9;
    }

    private static void printArray(int[][] sudoku) {
        for (int i = 0; i < sudoku.length; i++) {
            for (int j = 0; j < sudoku[i].length; j++) {
                System.out.printf("%6s", sudoku[i][j]);
            }
            System.out.println();
        }
    }
}
