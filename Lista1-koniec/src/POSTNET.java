import java.util.Scanner;

public class POSTNET {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String strings = scanner.next();
        System.out.println("*****");
        for (int counter = 0; counter < strings.length(); counter++) {
            int number = Integer.parseInt(String.valueOf(strings.charAt(counter)));
            generateCode(number);
        }
    }

    private static void generateCode(int numer) {
        switch (numer){
            case 0:{
                System.out.println("*****\n" +
                        "*****\n" +
                        "**\n" +
                        "**\n**");
                break;
            }
            case 1:{
                System.out.println("**\n" +
                        "**\n" +
                        "**\n" +
                        "*****\n*****");
                break;
            }
            case 2:{
                System.out.println("**\n" +
                        "**\n" +
                        "*****\n" +
                        "**\n*****");
                break;
            }
            case 3:{
                System.out.println("**\n" +
                        "**\n" +
                        "*****\n" +
                        "*****\n**");
                break;
            }
            case 4:{
                System.out.println("**\n" +
                        "*****\n" +
                        "**\n" +
                        "**\n*****");
                break;
            }
            case 5:{
                System.out.println("*****\n" +
                        "**\n" +
                        "**\n" +
                        "**\n**");
                break;
            }
            case 6:{
                System.out.println("**\n" +
                        "*****\n" +
                        "*****\n" +
                        "**\n**");
                break;
            }
            case 7:{
                System.out.println("*****\n" +
                        "**\n" +
                        "**\n" +
                        "**\n*****");
                break;
            }
            case 8:{
                System.out.println("*****\n" +
                        "**\n" +
                        "**\n" +
                        "*****\n**");
                break;
            }
            case 9:{
                System.out.println("*****\n" +
                        "**\n" +
                        "*****\n" +
                        "**\n**");
                break;
            }

        }
        System.out.println("*****");
    }
}
