import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Locker {
    static Scanner scanner = new Scanner(System.in);
    static Boolean[] booleansArray = new Boolean[101];

    public static void main(String[] args) {
        Arrays.fill(booleansArray ,false);


        for (int counterDzielnik = 2; counterDzielnik <= 100; counterDzielnik++) {
            for (int index = 1; index <=100; index++) {
                if (index%counterDzielnik==0){
                    closeOrOpen(index);
                }
            }

        }

        printArray();
    }

    private static void printArray() {
        for (int i = 0; i < booleansArray.length; i++) {
            if (!booleansArray[i]){
                System.out.println(i + " is CLOSED");
            }
        }
    }

    private static void closeOrOpen(int counter) {
            if (booleansArray[counter]){
                booleansArray[counter]=false;
            }
            else {
                booleansArray[counter]=true;
            }

    }
}
