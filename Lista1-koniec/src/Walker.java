import java.sql.SQLOutput;
import java.util.Random;
import java.util.Scanner;

public class Walker {
    static Scanner scanner = new Scanner(System.in);
    static Random random = new Random();
    static Boolean[][] putBool;
    static Boolean[][] arrayWithBool;

    public static void main(String[] args) {
        System.out.println("Size od array 10:100");
        int size = scanner.nextInt();
        System.out.println("Punkt START");
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        arrayWithBool = new Boolean[size][size];
        arrayWithBool = putBool(size);
        walkerTime(x,y);
    }

    private static void printArray() {
        for (int counterX = 0; counterX < arrayWithBool.length; counterX++) {
            for (int counterY = 0; counterY < arrayWithBool[counterX].length; counterY++) {
                System.out.printf("%6s", arrayWithBool[counterX][counterY]);
            }
            System.out.println();
        }
    }

    private static void walkerTime(int x, int y) {
        arrayWithBool[x][y] = true;
        int result = 0;
        while(!isAllTrue()) {
            int decision = random.nextInt(3);
            switch (decision) {
                case 0: {
                    y++;
                    break;
                }
                case 1: {
                    y--;
                    break;
                }
                case 2: {
                    x--;
                    break;
                }
                case 3: {
                    x++;
                    break;
                }
            }
            try{
                arrayWithBool[x][y]=true;
                result++;
            }catch (IndexOutOfBoundsException e){
                return;
            }
        }
        System.out.println(result);
    }

    private static boolean isAllTrue() {
        for (int i = 0; i < arrayWithBool.length; i++) {
            for (int j = 0; j < arrayWithBool[i].length; j++) {
                if (!arrayWithBool[i][j]);
                return false;
            }
        }
        return true;
    }


    private static Boolean[][] putBool(int size) {
        Boolean[][] arrayBoolean = new Boolean[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                arrayBoolean[i][j]=false;
            }
        }
        return arrayBoolean;
    }
}
