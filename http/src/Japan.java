import org.json.JSONArray;
import org.json.JSONObject;

public class Japan {
    private static String japan ="[{\"name\":\"Japan\",\"topLevelDomain\":[\".jp\"],\"alpha2Code\":\"JP\",\"alpha3Code\":\"JPN\",\"callingCodes\":[\"81\"],\"capital\":\"Tokyo\",\"altSpellings\":[\"JP\",\"Nippon\",\"Nihon\"],\"region\":\"Asia\",\"subregion\":\"Eastern Asia\",\"population\":126960000,\"latlng\":[36.0,138.0],\"demonym\":\"Japanese\",\"area\":377930.0,\"gini\":38.1,\"timezones\":[\"UTC+09:00\"],\"borders\":[],\"nativeName\":\"日本\",\"numericCode\":\"392\",\"currencies\":[{\"code\":\"JPY\",\"name\":\"Japanese yen\",\"symbol\":\"¥\"}],\"languages\":[{\"iso639_1\":\"ja\",\"iso639_2\":\"jpn\",\"name\":\"Japanese\",\"nativeName\":\"日本語 (にほんご)\"}],\"translations\":{\"de\":\"Japan\",\"es\":\"Japón\",\"fr\":\"Japon\",\"ja\":\"日本\",\"it\":\"Giappone\",\"br\":\"Japão\",\"pt\":\"Japão\",\"nl\":\"Japan\",\"hr\":\"Japan\",\"fa\":\"ژاپن\"},\"flag\":\"https://restcountries.eu/data/jpn.svg\",\"regionalBlocs\":[],\"cioc\":\"JPN\"}]";

    public static void main(String[] args) {
        JSONArray array = new JSONArray(japan);
        JSONObject jsonObject = (JSONObject) array.get(0);
        System.out.println("Nazwa; "+ jsonObject.get("name"));
        System.out.println("Area: "+ jsonObject.get("area"));
        System.out.println("Liczba ludności:" + jsonObject.get("population"));
        JSONArray borders  = jsonObject.getJSONArray("borders");
        for (Object o: borders) {
            System.out.println(o);
        }
        JSONArray altSpellings = jsonObject.getJSONArray("altSpellings");
        for (Object o :
                altSpellings) {
            System.out.println(o);
        }
        System.out.println("cioc "+ jsonObject.get("cioc"));

        JSONObject jsonObject1;
    }
}
