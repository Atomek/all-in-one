import java.util.Scanner;

public class Ostern {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int a, b, c, d, e, f, g, h, i, j, k, l, m, n, p;
        System.out.println("Podaj rok: ");
        int year = scanner.nextInt();

        a = year%19;
        b = (int) Math.floor(year/100);
        c = year%100;
        d = (int) Math.floor(b/4);
        e = b%4;
        f = (int) Math.floor((b+8)/25);
        g = (int) Math.floor((b-f+1)/3);
        h = (19*a+b-d-g+15)%30;
        i = (int) Math.floor(c/4);
        k = c%4;
        l = (32+(2*e)+(2*i)-h-k)%7;
        m = (int) Math.floor((a+(11*h)+(22*l))/451);
        p = (h+l-(7*m)+114)%31;

        int day = p+1;
        int preMonth = (int) Math.floor((h+l-(7*m)+114)/31);

        if (preMonth==4){
            System.out.println("Wielkanoc w " + year + " bedzie: " + day + " kwietnia");
        }
        else{
            System.out.println("Wielkanoc w " + year + " bedzie: " + day + " marca");
        }


    }
}
