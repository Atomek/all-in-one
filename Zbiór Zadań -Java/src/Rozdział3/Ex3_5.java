package Rozdział3;

public class Ex3_5 {
    public static void main(String[] args) {
        Character programowanie[] = {'p','r','o','g','r','a','m','o','w','a','n','i','e'};

        int i=programowanie.length-1;
        for (int counter = 0; counter < programowanie.length / 2; counter++) {
            programowanie[counter]=programowanie[i];
            i--;
        }

        for (Character aha :
                programowanie) {
            System.out.print(aha);
        }
    }
}
