package Rozdział3;

public class Ex3_2 {
    public static void main(String[] args) {
        Character character[] = {'I','n','f','o','r','m','a','t','y','k','a'};

        for (int counter = 0; counter < character.length; counter++) {
            System.out.println(character[counter]);
        }

        for (int counter = 0; counter < character.length; counter++) {
            System.out.print(character[counter]+ " ");
        }

        System.out.println();

        for (int counter = 0; counter < character.length; counter++) {
            System.out.print( Character.toUpperCase(character[counter]));
        }

        System.out.println();

        for (int counter = 0; counter < character.length; counter++) {
            System.out.print(Character.toLowerCase(character[counter]));
        }
    }
}
