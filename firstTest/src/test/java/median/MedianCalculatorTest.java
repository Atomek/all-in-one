package median;

import com.median.MedianCalculator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;


public class MedianCalculatorTest {
    @DisplayName(":)")
    @ParameterizedTest
    @MethodSource("testDataGenerator")
    void divide(int[] array, int expectedresult) {
        int actual = MedianCalculator.getMedian(array);
        Assertions.assertThat(actual).isEqualTo(expectedresult);
    }


    static List<Arguments> testDataGenerator() {
        Arguments argument1 = Arguments.of(new int[]{1, 2, 3, 4, 5}, 3);
        Arguments argument2 = Arguments.of(new int[]{},-1);
        Arguments argument3 = Arguments.of(new int[]{1, 2, 3, 4}, 2);
        Arguments argument4 = Arguments.of(new int[]{2,7}, 4);
        Arguments argument5 = Arguments.of(null, -1);
        Arguments argument6 = Arguments.of(new int[]{5, 7, 5, 6, 6, 6}, 6);
        return Arrays.asList(argument1, argument2, argument3, argument4, argument5,argument6);
    }

}

