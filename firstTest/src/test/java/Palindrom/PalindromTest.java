package Palindrom;

import com.gitlab.Atomek.PalindromMustBeExeption;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import palindrom.Palindrom;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PalindromTest {
    private Palindrom palindrom;

    @BeforeEach
    public void setUp() {
        palindrom= new Palindrom();
    }

    @Test
    void isPalindromTest() {
        //given
        String kajak = "xx";
        //then
        assertTrue(palindrom.isPalindrom(kajak));
    }

    @ParameterizedTest
    @ValueSource(strings = {"222", "kajak", "Kobyla ma maly bok"})
    void isPalindromParameterized(String word) {
        assertTrue(palindrom.isPalindrom(word));
    }

    static List<Arguments> testDataString() {
        Arguments argument1 = Arguments.of("kajak", true);
        Arguments argument2 = Arguments.of("aa1", false);
        Arguments argument3 = Arguments.of("Kobyła ma mały bok", true);
        Arguments argument4 = Arguments.of("k", true);
        return Arrays.asList(argument1, argument2, argument3, argument4);
    }

    @ParameterizedTest
    @MethodSource("testDataString")
    void isPalindromMethodSource(String string, boolean isPali) {

        boolean actual = palindrom.isPalindrom(string);
        assertEquals(isPali, actual);


    }

    @Test
    void isPalindromExe(){
        try{
            palindrom.isPalindrom(null);
        }catch (PalindromMustBeExeption exeption){
            assertEquals(exeption.getMessage(), "Error");
        }
    }

}
