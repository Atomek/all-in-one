package com.gitlab.Atomek;


import org.assertj.core.api.Assert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;



public class AssetJTest {
    private Calculator calculator;
    @BeforeEach
    public void setUp() {
        calculator = new Calculator();
    }

    @Test
    void divide() {
        int a =1;
        int b =1;
        int expectedresult = 1;
        int actual = calculator.divide(a, b);
        Assertions.assertThat(actual)
                .isEqualTo(expectedresult)
                .isLessThan(20)
                .inHexadecimal()
                .isPositive()
                .isBetween(0, 10)
                .isEqualTo(1)
                .isOne()
                .describedAs("1");

    }
}
