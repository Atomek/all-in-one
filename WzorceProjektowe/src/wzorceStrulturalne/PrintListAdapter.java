package wzorceStrulturalne;

import java.util.Map;


public class PrintListAdapter implements Printalble {
    private Map<String, String> rakMap;

    public PrintListAdapter(Map<String, String> rakMap) {
        this.rakMap = rakMap;
    }


    @Override
    public void print() {
        for (Map.Entry<String, String> entry : rakMap.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }

    }


}
