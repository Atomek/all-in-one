package wzorceStrulturalne;

import java.util.HashMap;
import java.util.Map;

public class PrintListMain {
    public static void main(String[] args) {
        PrintListAdapter printListAdapter = new PrintListAdapter(setRakMap());
        printListAdapter.print();
    }

    static Map setRakMap() {
        Map<String, String> rakMap = new HashMap<>();
        rakMap.put("si", "1");
        rakMap.put("s2", "5");
        rakMap.put("s3", "4");
        rakMap.put("s4", "3");
        rakMap.put("s5", "2");
        return rakMap;
    }
}
