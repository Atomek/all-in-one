package drinkee;

public class Czeko extends SkładnikiDecorator{
    Napoj napoj;

    @Override
    double koszt() {
        return napoj.koszt()+1;
    }

    @Override
    public String pobierzOpis() {
        return napoj.pobierzOpis()+ " z czekolado";
    }
}
