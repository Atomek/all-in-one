package drinkee;

public class Kawiarnia {
    public static void main(String[] args) {
        Espresso espresso = new Espresso();
        Mleko mleko = new Mleko(espresso);
        Mleko mleko2 = new Mleko(mleko);
        System.out.println(mleko2.pobierzOpis());
        System.out.println(mleko2.koszt());
    }
}
