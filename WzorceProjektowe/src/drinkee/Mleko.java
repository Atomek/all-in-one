package drinkee;

public class Mleko extends SkładnikiDecorator {
    Napoj napoj;

    public Mleko(Napoj napoj){
        this.napoj=napoj;
    }

    @Override
    double koszt() {
        return napoj.koszt()+0.5;
    }

    @Override
    public String pobierzOpis() {
        return napoj.pobierzOpis()+" z mlekiem";
    }
}
