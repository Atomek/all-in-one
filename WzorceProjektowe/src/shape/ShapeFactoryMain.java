package shape;

import java.util.Random;

public class ShapeFactoryMain {
    public static void main(String[] args) {
        System.out.println(giveShape().draw());
    }
    public static Shape giveShape(){
        Random generator = new Random();
        return ShapeFactory.createShape(generator.nextInt(3));
    }
}
