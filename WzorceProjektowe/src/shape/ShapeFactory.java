package shape;

import java.util.Random;

public class ShapeFactory {

    public static Shape createShape(int type) {
        switch (type){
            case 0:
                return new Circle();
            case 1:
                return new Triangle();
            case 2:
                return new Squere();
                default:
                    throw new IllegalArgumentException();

        }

    }


}
