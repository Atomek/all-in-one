package generic;
import java.util.Scanner;

public class BoxMain {
    public static void main(String[] args) {
        GenericBox<String> stringGenericBox = new GenericBox<>();
        GenericBox<Integer> integerGenericBox = new GenericBox<>();
        GenericBox<Boolean> booleanGenericBox = new GenericBox<>();
        String string = getString();
        putValue(stringGenericBox, integerGenericBox, booleanGenericBox, string);
        printIt(stringGenericBox, integerGenericBox, booleanGenericBox);
    }

    private static void printIt(GenericBox<String> stringGenericBox, GenericBox<Integer> integerGenericBox, GenericBox<Boolean> booleanGenericBox) {
        System.out.println(stringGenericBox.toString() + "\n" + integerGenericBox.toString() + "\n" + booleanGenericBox.toString());
    }

    private static void putValue(GenericBox<String> stringGenericBox, GenericBox<Integer> integerGenericBox, GenericBox<Boolean> booleanGenericBox, String string) {
        stringGenericBox.set(string);
        integerGenericBox.set(string.length());
        checkBoolean(booleanGenericBox, string);
    }

    private static String getString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    private static void checkBoolean(GenericBox<Boolean> booleanGenericBox, String string) {
        if (string.length() % 2 == 0) {
            booleanGenericBox.set(true);
        } else {
            booleanGenericBox.set(false);
        }
    }
}
