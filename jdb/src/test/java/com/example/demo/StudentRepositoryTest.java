package com.example.demo;

import com.example.demo.Repositories.CourseRepository;
import com.example.demo.Repositories.StudentRepository;
import com.example.demo.entities.Course;
import com.example.demo.entities.Student;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class StudentRepositoryTest {@Autowired
EntityManager em;  //nawiązanie połaczenia z bazą
    @Autowired
    StudentRepository studentRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    public void findByIdTest(){
        Student student = studentRepository.findById(30001L);
        Assert.assertEquals("Papryk1", student.getName());
    }




}
