package com.example.demo;

import com.example.demo.entities.Course;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class JPQLTest {
    @Autowired
    EntityManager em;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    public void getAllCoursesTest(){
        List courses = em.createQuery(
                "SELECT c FROM Course c"
        ).getResultList();
        logger.info("Kursy -> {}",courses);
    }
    @Test
    public void getAllCoursesWithTestTest(){
        List courses = em.createQuery(
                "SELECT c FROM Course c where c.name like '%Test%'"
        ).getResultList();
        logger.info("Kursy -> {}",courses);
    }
    @Test
    public void getAllCoursesWith3Test(){
        List courses = em.createQuery(
                "SELECT c FROM Course c where c.name like '%3%'"
        ).getResultList();
        logger.info("Kursy -> {}",courses);
    }
    @Test
    public void getAllCoursesWithBETTest(){
        List courses = em.createQuery(
                "SELECT c FROM Course c where c.id between 10003 and 10005"
        ).getResultList();
        logger.info("Kursy -> {}",courses);
    }
    @Test
    public void getAllCoursesByNamedQuery(){
        List courses = em.createQuery("select_all").getResultList();
        logger.info("Kursy -> {}",courses);
    }


}
