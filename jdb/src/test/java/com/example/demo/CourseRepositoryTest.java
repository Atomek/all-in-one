package com.example.demo;

import com.example.demo.Repositories.CourseRepository;
import com.example.demo.entities.Course;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class CourseRepositoryTest {
    @Autowired
    EntityManager em;  //nawiązanie połaczenia z bazą
    @Autowired
    CourseRepository courseRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    public void findByIdTest(){
        Course course = courseRepository.findById(10001L);
        Assert.assertEquals("Testowo", course.getName());
    }

    @Test
    @DirtiesContext
    public void deleteById(){
        courseRepository.deleteById(10002L);
        Assert.assertNull(courseRepository.findById(10002L));
    }

    @Test
    @DirtiesContext
    public void insertById(){
        if (courseRepository.findById(1L) == null){
            Course course = new Course("Ok");
            courseRepository.save(course);

        }
        Assert.assertNotNull(courseRepository.findById(1L));
    }

    @Test
    @DirtiesContext
    public void updateById() {
        Course course = courseRepository.findById(10003L);
        Assert.assertEquals("Testowo3", course.getName());


        course.setName("Testowo_po_zmianie");
        courseRepository.save(course);
        Course courseAfterUpdate = courseRepository.findById(10003L);
        Assert.assertEquals(courseAfterUpdate.getName(),"Testowo_po_zmianie");
    }

    @Test
    @DirtiesContext
    public void playWithEM() {
        courseRepository.playWithEM();
    }




}
