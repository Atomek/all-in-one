package com.example.demo;

import com.example.demo.Repositories.PassportRepository;
import com.example.demo.entities.Passport;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class PassportRepositoryTest {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    PassportRepository passportRepository;

    @Test
    public void getById(){
        Passport passport = passportRepository.getById(20001L);
        Assert.assertEquals("20001", passport.getNumber());
    }

}
