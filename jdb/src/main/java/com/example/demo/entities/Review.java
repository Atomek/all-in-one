package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Struct;

@Entity
public class Review {
    @Id
    @GeneratedValue
    private Long Id;

    private String rating;
    private String description;


    protected Review(){}

    public Review(String rating, String description){
        this.rating=rating;
        this.description=description;
    }

    public Long getId() {
        return Id;
    }

    public String getRating() {
        return rating;
    }

    public String getDescription() {
        return description;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Review{" +
                "Id=" + Id +
                ", rating='" + rating + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
