package com.example.demo.entities;

import javax.persistence.*;

@Entity
@Table(name = "course")
@NamedQueries(value = {@NamedQuery(name = "select_all", query = "SELECT c FROM Course c"), @NamedQuery(name = "select_all_with_test",
        query = "SELECT c FROM Course c where c.name like '%3%'")

})

public class Course {
    //kodFirst najlpierw tworzone są klasy poźniej tabele
    @Id                 //klucz głowny
    @GeneratedValue     //Hibernate ma się zatroszczyć zeby klucz głowny ma byc niepowtarzalny
    private Long id;
    @Column(name = "name", nullable = false)
    private String name;

    protected Course() {
    }

    public Course(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
