package com.example.demo.Repositories;

import com.example.demo.entities.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class StudentRepository {
    @Autowired
    EntityManager em;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Student findById(long id) {
        return em.find(Student.class, id); //(co ma wzracać, względem czego)
    }

    public void deleteById(long id) {
        Student student = findById(id);
        em.remove(student);
    }

    public Student save(Student student) {
        if (student.getId() == null) {
            em.persist(student);     //insert
        } else {
            em.merge(student);       //update
        }
        return student;
    }


}

