package com.example.demo.Repositories;

import com.example.demo.entities.Review;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class ReviewRepository {
    public class CourseRepository {
        @Autowired
        EntityManager em;
        Logger logger = LoggerFactory.getLogger(this.getClass());

        public Review findById(long id) {
            return em.find(Review.class, id); //(co ma wzracać, względem czego)
        }

        public void deleteById(long id) {
            Review review = findById(id);
            em.remove(review);
        }

        public Review save(Review review) {
            if (review.getId() == null) {
                em.persist(review);     //insert
            } else {
                em.merge(review);       //update
            }
            return review;
        }


    }
}
