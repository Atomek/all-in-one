package com.example.demo.Repositories;

import com.example.demo.entities.Course;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class CourseRepository {
    @Autowired
    EntityManager em;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Course findById(long id){
        return em.find(Course.class, id); //(co ma wzracać, względem czego)
    }

    public void deleteById(long id){
        Course course = findById(id);
        em.remove(course);
    }

    public Course save(Course course){
        if(course.getId() == null){
            em.persist(course);     //insert
        }
        else{
            em.merge(course);       //update
        }
        return course;
    }

    public void playWithEM(){
        logger.info("Jestem w metodzie");


        Course course = new Course("AAAAAAA");
        em.persist(course);
        em.flush();
        course.setName("BBBBBB");

em.refresh(course);
    }

}
