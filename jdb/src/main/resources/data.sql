insert into Course(id, name) values(10001, 'Testowo');
insert into Course(id, name) values(10002, 'Testowo2');
insert into Course(id, name) values(10003, 'Testowo3');
insert into Course(id, name) values(10004, 'Testowo4');
insert into Course(id, name) values(10005, 'Testowo5');


insert into Passport(id, number) values(20001, '20001');
insert into Passport(id, number) values(20002, '20002');
insert into Passport(id, number) values(20003, '20003');
insert into Passport(id, number) values(20004, '20004');
insert into Passport(id, number) values(20005, '20005');


insert into Student(id, name) values(30001, 'Papryk1');
insert into Student(id, name) values(30002, 'Papryk2');
insert into Student(id, name) values(30003, 'Papryk3');
insert into Student(id, name) values(30004, 'Papryk4');
insert into Student(id, name) values(30005, 'Papryk5');


insert into Review(id, rating, description) values(40001, '1', 'bad');
insert into Review(id, rating, description) values(40002, '2', 'bad-ok');
insert into Review(id, rating, description) values(40003, '3', 'ok');
insert into Review(id, rating, description) values(40004, '4', 'ok-good');
insert into Review(id, rating, description) values(40001, '5', 'good');
