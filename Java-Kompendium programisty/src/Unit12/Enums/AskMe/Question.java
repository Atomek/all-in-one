package Unit12.Enums.AskMe;

import java.util.Random;

public class Question {
    Random random = new Random();

    Answers ask(){

        int pb = random.nextInt(100);

        if(pb<15) return Answers.MAYBE;
        if (pb<30) return Answers.NO;
        if (pb<60) return Answers.YES;
        if (pb<75) return Answers.LATER;
        if (pb<98) return Answers.SOON;
        else return Answers.NEVER;

    }
}
