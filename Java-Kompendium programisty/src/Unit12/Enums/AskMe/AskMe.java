package Unit12.Enums.AskMe;

public class AskMe {
    static void answer(Answers answers){
        switch (answers){
            case SOON:
                System.out.println(Answers.SOON);
                break;
            case LATER:
                System.out.println(Answers.LATER);
                break;
            case NO:
                System.out.println(Answers.NO);
                break;
            case YES:
                System.out.println(Answers.YES);
                break;
            case MAYBE:
                System.out.println(Answers.MAYBE);
                break;
            case NEVER:
                System.out.println(Answers.NEVER);
                break;
        }
    }
}
