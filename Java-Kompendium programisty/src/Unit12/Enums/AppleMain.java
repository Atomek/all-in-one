package Unit12.Enums;

public class AppleMain {
    public static void main(String[] args) {
        Apple ap;
        ap=Apple.REDDEL;

        System.out.println(ap.ordinal());               //orinal() - kolejność stałej na liście
        System.out.println(ap.compareTo(Apple.REDDEL));     //porównanie kolejności dwóch stałych jeżeli te samo miejsce
                                                            // wtedy 0 jeżeli większe to >0 jeżeli mniejsze to <0
        System.out.println(ap.compareTo(Apple.CORLAND));

        System.out.println(ap.equals(Apple.REDDEL));
        System.out.println(ap.equals(Apple.CORLAND));


        System.out.println(ap);

        if (ap==Apple.REDDEL){
            System.out.println("true");
        }


        switch (ap){
            case REDDEL:{
                System.out.println(Apple.REDDEL);
                break;
            }
            case CORLAND:{
                System.out.println(Apple.CORLAND);
                break;
            }
            case JONATHAN:{
                System.out.println(Apple.JONATHAN);
                break;
            }
            case WINESAP:{
                System.out.println(Apple.WINESAP);
                break;
            }
            case GOLDENDEL:{
                System.out.println(Apple.GOLDENDEL);
                break;
            }
        }

        for (Apple apple :
                Apple.values()) {
            System.out.print(apple + " ");
        }

        System.out.println();

        System.out.println(AppleV1.CORLAND.getPrice());


        for (AppleV1 aps :
                AppleV1.values()) {
            System.out.println(aps.getPrice());
        }

        System.out.println(AppleV1.valueOf("CORLAND").getPrice());



    }






}
