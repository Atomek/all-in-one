package Unit12.Enums;

public enum  AppleV1 {

    JONATHAN(2), GOLDENDEL(4), REDDEL(6), WINESAP(6), CORLAND(19);

    private int price;


    AppleV1(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

}
