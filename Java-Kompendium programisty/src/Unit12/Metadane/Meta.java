package Unit12.Metadane;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

// Deklaracja typu adnotacji.
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnno {
    String str();
    int val();
}

class Meta {

    // Adnotacja metody.
    @MyAnno(str = "Przyk�adowa adnotacja", val = 100)
    public static void myMeth() {
        Meta ob = new Meta();

        // Pobranie adnotacji dla tej metody i
        // wy�wietlenie warto�ci jej sk�adowych.
        try {
            // Najpierw pobierz obiekt Class reprezentuj�cy
            // aktualn� klas�.
            Class c = ob.getClass();

            // Pobierz obiekt Method reprezentuj�cy
            // aktualn� metod�.
            Method m = c.getMethod("myMeth");


            // Nast�pnie pobierz adnotacj� dla tej metody.
            MyAnno anno = m.getAnnotation(MyAnno.class);

            // Wy�wietl warto�ci adnotacji.
            System.out.println(anno.str() + " " + anno.val());
        } catch (NoSuchMethodException exc) {
            System.out.println("Nie znaleziono metody.");
        }
    }

    public static void main(String args[]) {
        myMeth();
    }
}
