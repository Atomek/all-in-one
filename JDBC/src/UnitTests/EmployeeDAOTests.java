package UnitTests;

import DAOImplementation.DAOs.EmployeeDAO;
import DAOImplementation.Entities.Employee;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class EmployeeDAOTests {

    @Test
    public void insertEmployeeTest() throws Exception {
        Employee employee =
                new Employee(999, "AAA",
                        "BBB", 1);

        EmployeeDAO dao = new EmployeeDAO();
        List<Employee> pracownicy = dao.select();
        for (Employee employeeList : pracownicy) {
            if(employeeList.getId() == employee.getId()){
                throw new Exception("Jest źle");
            }
        }

        dao.insert(employee);
        boolean isOk = false;
        List<Employee> pracownicyPoInsercie = dao.select();
        for (Employee employeeList : pracownicyPoInsercie) {
            if(employeeList.getId() == employee.getId()){
                isOk = true;
            }
        }
        Assert.assertTrue(isOk);

    }

}
