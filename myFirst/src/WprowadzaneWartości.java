import java.util.Scanner;  // użycie klasy Scanner odwołuje się do klasy java.util

    public class WprowadzaneWartości {

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);  // utworzenie obiektu klasy Scanner
        String line;
        System.out.println("Podaj swoje imie: ");
        line = in.nextLine();              // pobranie ciągu wpisanych znaków
        System.out.println("Właśnie wprowadziłes " + line);
    }
    }
