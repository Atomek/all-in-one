public class Loops {
    public static void main(String[] args) {
        doWhileLoop();
        int counter = 0;

        while (counter < 500) {
            counter = counter + 1;
            System.out.println(counter);
        }
        forLoop();
    }

    static void doWhileLoop(){                      //pętla w metodzie
        int counter = 0;
        do {

            counter = counter +1;
            System.out.println(counter);

        } while(counter < 10);
    }

    static void forLoop(){
        for(int x=900; x<=932; x=x+1){      //najpierw sprawdzany warunek pożniej wykonana instrukcja
            System.out.println(x);
        }
    }

}
