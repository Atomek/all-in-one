
public class Hello {
    public static void main(String[] args){
        System.out.println("Hello, World"); //println po wypisaniu ciągu znaków przenosi kursor do nowej lini

        System.out.print("Hello! "); // print po wypisaniu znaków pozostawia kursor na lini wypisania
        System.out.print("How Are You \n"); // \n sekwencja ucieczki [do nowej lini]
        System.out.println("I`m \"fine\"."); // \" znak cudzysłowia

                                            // \t Tabulacja
                                            // \\ ukośnik

        System.out.println(2 + " Hello " + 5);
        System.out.println(2 + 5 + " Hello");
    }
}
