import java.awt.*;

public class Conditionals {
    public static void main(String[] args){

        boolean sayHello = true; // nadajemy zmiennym wynik
        boolean sayGoodbye = false;


        //if(sayHello){
        //System.out.println("Hello");}

        if(sayGoodbye){                         //stwierdzenie jest fałszywe wiec omijamy nastepną instukcje
        System.out.println("Goodbye");}         //i przechodzimy to tego co jest pod else
        else if(sayGoodbye){
            System.out.println("say something");}
        else System.out.println("Hello");

        int x = 5;
                            // Operatory arytmetyczne w if`ach <> <= >= == (rowne) !=(rózne od)
                            // Opertory jeżeli jest wiecej warunków
                            // AND && np (x>5 && x<6) znaczy ze jeżeli liczba x jest wieksza od 5 i mniejsza od 6 to
                            // warunek jest prawdziwy
                            // OR || np x=4 || x>6 znaczy ze jezeli liczba x jest równa 4 lub jest wieksza od 6 to
                            // warunek jest prawdziwy

        if(8!=x){
            System.out.println(x);
        }

        }
    }

