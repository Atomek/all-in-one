public class Angle {

    private double x;

    public Angle(double x) {
        this.x = x;
    }

    public double getX() {
        return x;
    }


    public void setX(double x) {
        this.x = x;
    }

    public double sin() {
        return Math.sin(x);
    }

    public double cos() {
        return Math.cos(x);
    }

    public double tan() {
        return Math.tan(x);
    }

    public double ctan() {
        return 1.0 / Math.tan(x);
    }

    public double arccos() {
        return 1.0 / Math.cos(x);
    }

    public double arcsin() {
        return 1.0 / Math.cos(x);
    }

    public double radian() {
        return x * Math.PI / 180;
    }

    public double degree() {
        return 180 * x / Math.PI;
    }

    public void toString(double degree) {
        int stopnie = (int) degree;
        String min = Double.toString(degree - stopnie);
        int min1 = (Character.getNumericValue(min.charAt(3))) * 10 + (Character.getNumericValue(min.charAt(4)));
        int sek = (Character.getNumericValue(min.charAt(5))) * 10 + (Character.getNumericValue(min.charAt(6)));
        System.out.print(stopnie + "o" + min1
                + "\'" + sek + "\"");
    }

    private int degree;
    private int minutes;
    private int seconds;

    public Angle(int degree, int minutes, int seconds) {
        if (degree < 360 && degree > 0 && minutes < 60 && seconds < 60) {
            this.degree = degree;

            this.minutes = minutes;

            this.seconds = seconds;
        }
    }

    public Angle(int degree, int minutes) {
        if (degree < 360 && degree > 0 && minutes < 60){
            this.degree = degree;
            this.minutes = minutes;
        }

    }

    public Angle(int degree) {
        if (degree < 360)
        this.degree = degree;
    }
}
